var renderUtils = (function () {

    function createElement(elementType, elementClass) {
        var element = document.createElement(elementType);
        if (elementClass !== null) {
            element.setAttribute("class", elementClass);
        }
        return element;
    }

    function setElementClass(elementID, elementClass) {
        var contentDiv = document.getElementById(elementID, elementClass);
        contentDiv.setAttribute("class", elementClass);
    }

    function createLabelAndContent(elementLabelText, elementContentText, elementClass) {
        var label = createElement("label", "content-label");
        label.innerHTML = label.innerHTML + elementLabelText;
        var content = createElement("p", null);
        content.innerHTML = content.innerHTML + elementContentText;
        label.appendChild(content);
        return label;
    }

    return {
        createElement: createElement,
        setElementClass: setElementClass,
        createLabelAndContent: createLabelAndContent
    }
})();