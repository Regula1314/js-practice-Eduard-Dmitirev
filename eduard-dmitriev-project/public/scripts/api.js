
var goodsApi = (function () {

    var BASE_URL = "/api/goods";
    var headers = new Headers({
        "Content-Type": "application/json"
    });

    function sendJSONPostRequest(body) {
        var headers = new Headers({
            "Content-Type": "application/json"
        });
        return fetch(BASE_URL, {
            method: "POST",
            headers: headers,
            body: body
        });
    }


    function returnJson(response) {
        return response.json();
    }

    function getAllGoods() {
        return fetch(BASE_URL).then(returnJson);
    }

    function getGoodsByCategory(category){
        return fetch(BASE_URL+"/"+category.id).then(returnJson);
    }
    function addGood(good) {
        return sendJSONPostRequest(good).then(returnJSON)
    }
    return {
        getAllGoods: getAllGoods,
        addGood: addGood,
        getGoodsByCategory: getGoodsByCategory
    }

})();

var categoriesApi = (function () {

    var BASE_URL = "/api/categories";
    var headers = new Headers({
        "Content-Type": "application/json"
    });

    function returnJson(response) {
        return response.json();
    }

    function getAllCategories() {
        return fetch(BASE_URL).then(returnJson);
    }

    return {
        getAllCategories: getAllCategories
    }

})();