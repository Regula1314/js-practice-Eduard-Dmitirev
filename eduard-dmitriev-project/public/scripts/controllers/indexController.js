

var indexController = (function (categoriesApi, goodsApi, renderUtils) {

    function clearContentDiv() {
        var contentDiv = document.getElementById("content-panel-content-area");
        while (contentDiv.childElementCount > 0) {
            contentDiv.removeChild(contentDiv.firstChild);
        }
    }

    function catageoryListElementOnClickEvent(category) {
        goodsApi.getGoodsByCategory(category).then(renderListOfGoods).catch(renderError);
        //(category);
        //console.log(category.name);
    }

    function renderCategoryListElement(category, index, arr) {
        var li = document.createElement('li', catageoryListElementOnClickEvent);

        li.addEventListener("click", function () {
            catageoryListElementOnClickEvent(category)
        }, false);
        // li.setAttribute('class','item');
        var ul = document.getElementById("nav-panel-list");
        ul.appendChild(li);
        li.innerHTML = li.innerHTML + category.name;
    }

    function renderCategoryList(json) {
        var categoryList = json;
        categoryList.forEach(renderCategoryListElement);
    }

    function renderError(error) {
        console.log(error);
    }

    function initMainPanel() {
        categoriesApi.getAllCategories().then(renderCategoryList).catch(renderError);
        goodsApi.getAllGoods().then(renderContentTable).catch(renderError);
    }

    function renderContentTable(json) {
        clearContentDiv();
        renderUtils.setElementClass("content-panel-content-area", "content-area-as-table");
        var goodsList = json.slice(0, 12);
        goodsList.forEach(renderTableElement);
    }

    function renderTableElement(element, index, arr) {
        var tableElement = renderUtils.createElement("div", "content-table-item");

        var tableElementLeftDiv = renderUtils.createElement("div", "content-table-item-left");
        var tableElementRightDiv = renderUtils.createElement("div", "content-table-item-right");

        var elementHeader = renderUtils.createElement("p", "content-table-item-header");
        elementHeader.innerHTML = elementHeader.innerHTML + element.name;

        tableElementLeftDiv.appendChild(elementHeader);

        var figure = renderUtils.createElement("div", "content-table-item-image");
        var img = renderUtils.createElement("img");
        img.setAttribute("src", element.imgpath);
        figure.appendChild(img);
        //figure.innerHTML=figure.innerHTML+img.;

        tableElementLeftDiv.appendChild(figure);

        tableElementRightDiv.appendChild(renderUtils.createLabelAndContent("Категория:", element.category, null))
        tableElementRightDiv.appendChild(renderUtils.createLabelAndContent("Цена:", element.price, null))

        tableElement.appendChild(tableElementLeftDiv);
        tableElement.appendChild(tableElementRightDiv);

        var tableDiv = document.getElementById("content-panel-content-area");
        tableDiv.appendChild(tableElement);
    }

    function renderListOfGoods(json) {
        console.log(json);
        clearContentDiv();
        renderUtils.setElementClass("content-panel-content-area", "content-area-as-list");
        json.forEach(renderListElement);
    }

    function renderListElement(element, index, arr) {
        var listElement = renderUtils.createElement("div", "content-list-item");

        var listElementLeftDiv = renderUtils.createElement("div", "content-list-item-left");
        var listElementRightDiv = renderUtils.createElement("div", "content-list-item-right");

        var elementHeader = renderUtils.createElement("p", "content-list-item-header");
        elementHeader.innerHTML = elementHeader.innerHTML + element.name;

        listElementLeftDiv.appendChild(elementHeader);

        var figure = renderUtils.createElement("div", "content-list-item-image");
        var img = renderUtils.createElement("img");
        img.setAttribute("src", element.imgpath);
        figure.appendChild(img);
        //figure.innerHTML=figure.innerHTML+img.;

        listElementLeftDiv.appendChild(figure);

        listElementRightDiv.appendChild(renderUtils.createLabelAndContent("Категория:", element.category, null))
        listElementRightDiv.appendChild(renderUtils.createLabelAndContent("Цена:", element.price, null))

        listElement.appendChild(listElementLeftDiv);
        listElement.appendChild(listElementRightDiv);


        var contentDiv = document.getElementById("content-panel-content-area");
        contentDiv.appendChild(listElement);
    }

    return {
        initMainPanel: initMainPanel
    }
})(categoriesApi, goodsApi, renderUtils);



indexController.initMainPanel();
