var express = require('express');
var router = express.Router();
/* We start with a route '/', since the prefix would be maintained in app.js
*/
var goods = require('../service/goods');

router.get('/', function (req, res, next) {
    res.send(goods.getAllGoods());
});

function getCategoryFromURL(url) {
    return url.substr((url.lastIndexOf('/')) + 1, url.length);
}

router.get('/*', function (req, res, next) {
    console.log(goods.getGoodsByCategory(getCategoryFromURL(req.url)));
    res.send(goods.getGoodsByCategory(getCategoryFromURL(req.url)));
});

router.post('/', function (req, res, next) {
    var good = req.body;
    goods.addGood(good);
    res.send(good);
});
module.exports = router;