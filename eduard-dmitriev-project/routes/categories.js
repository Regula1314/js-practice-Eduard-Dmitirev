var express = require('express');
var router = express.Router();
/* We start with a route '/', since the prefix would be maintained in app.js
*/
var categories = require('../service/categories');

router.get('/', function(req, res, next) {
res.send(categories.getAllCategories());
});

module.exports = router;