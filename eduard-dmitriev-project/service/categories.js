var storage = [];

function getAllCategories() {
    return storage;
}
function getCategoryByID(id) {
    return storage.filter(function (item) {
        return (item.id == id);
    });
}

module.exports = {
    getAllCategories: getAllCategories,
    getCategoryByID: getCategoryByID
}

function setDefaultValues() {
    storage = [
        {
            "id": 1,
            "name": "Велосипеды"
        },
        {
            "id": 2,
            "name": "Мячи"
        }
    ]
}
setDefaultValues();