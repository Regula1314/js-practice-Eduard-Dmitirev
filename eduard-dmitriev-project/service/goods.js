var storage = [];
var categories = require('../service/categories');
function addGood(good) {
  storage.push(good);
}

function getAllGoods() {
  return storage;
}

function filterByCategory(item) {

}

function getGoodsByCategory(category) {  
  var categoryNames = categories.getCategoryByID(category);
  var categoryName;
  if (categoryNames.length!=1){

  } else{
    categoryName=categoryNames[0];  
  }
  return storage.filter(function (item) {
    return (item.category === categoryName.name);
  });
}

module.exports = {
  addGood: addGood,
  getAllGoods: getAllGoods,
  getGoodsByCategory:getGoodsByCategory
}

function setDefaultValues() {
  storage = [
    {
      "name": "Велосипед1",
      "category": "Велосипеды",
      "description": "Описание велосипеда",
      "imgpath": "images/img.png",
      "price": "100"
    },
    {
      "name": "Велосипед2",
      "category": "Велосипеды",
      "description": "Описание велосипеда",
      "imgpath": "images/img.png",
      "price": "100"
    },
    {
      "name": "Велосипед3",
      "category": "Велосипеды",
      "description": "Описание велосипеда",
      "imgpath": "images/img.png",
      "price": "100"
    },
    {
      "name": "Велосипед4",
      "category": "Велосипеды",
      "description": "Описание велосипеда",
      "imgpath": "images/img.png",
      "price": "100"
    }
  ]
}
setDefaultValues();